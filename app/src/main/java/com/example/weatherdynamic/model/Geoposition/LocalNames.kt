package com.example.weatherdynamic.model.Geoposition

data class LocalNames(
    val ar: String,
    val en: String,
    val fr: String,
    val ko: String,
    val ru: String,
    val uk: String
)