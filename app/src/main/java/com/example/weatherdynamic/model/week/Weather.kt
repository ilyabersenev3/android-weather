package com.example.weatherdynamic.model.week

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)