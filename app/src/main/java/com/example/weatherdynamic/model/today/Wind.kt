package com.example.weatherdynamic.model.today

data class Wind(
    val deg: Int,
    val speed: Double
)