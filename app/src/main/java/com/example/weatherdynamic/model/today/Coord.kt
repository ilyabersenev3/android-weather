package com.example.weatherdynamic.model.today

data class Coord(
    val lat: Int,
    val lon: Double
)