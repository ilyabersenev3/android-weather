package com.example.weatherdynamic.model.Geoposition

data class GeoposItem(
    val country: String,
    val lat: Double,
    val local_names: LocalNames,
    val lon: Double,
    val name: String,
    val state: String
)