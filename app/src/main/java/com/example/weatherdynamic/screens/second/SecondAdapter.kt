package com.example.weatherdynamic.screens.second

//import android.support.v7.widget.RecyclerView
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherdynamic.R
import com.example.weatherdynamic.model.week.week
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.android.synthetic.main.item_temperature_lauout.view.*
import java.text.SimpleDateFormat
import java.util.*

class SecondAdapter(diffCallback: DiffUtil.ItemCallback<WeatherModel>)
    :RecyclerView.Adapter<SecondAdapter.secondViewHolder>() {

    var listSecond = emptyList<week>()
    private lateinit var m_week: week

    class secondViewHolder(view: View): RecyclerView.ViewHolder(view){
        val date=view.findViewById<TextView>(R.id.item_date)
        val temperature=view.findViewById<TextView>(R.id.item_temperature)
        val humidity=view.findViewById<TextView>(R.id.item_humidity)
        val pressure=view.findViewById<TextView>(R.id.item_pressure)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): secondViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_temperature_lauout, parent, false)
        return secondViewHolder(view)
    }

    override fun onBindViewHolder(holder: secondViewHolder, position: Int) {

        val date = Date(m_week.current.dt *1000L)
        val sdf = SimpleDateFormat("dd.MM", Locale.getDefault())
        val formattedDate = sdf.format(date)

        holder.itemView.item_date.text = formattedDate
        holder.itemView.item_humidity.text = m_week.current.humidity.toString()+"%"
        holder.itemView.item_pressure.text = m_week.current.pressure.toString()
    }

    override fun getItemCount(): Int {
        return 5
    }

}