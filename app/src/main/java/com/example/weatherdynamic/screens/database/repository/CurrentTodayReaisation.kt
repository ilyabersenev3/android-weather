package com.example.weatherdynamic.screens.database.repository

import androidx.lifecycle.LiveData
import com.example.weatherdynamic.screens.database.dao.CurrentWeatherDaoToday
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModelToday


class CurrentTodayRealisation(private val currentWeatherDAO: CurrentWeatherDaoToday) {
    val currentWeather: LiveData<CurrentWeatherModelToday>
        get() = currentWeatherDAO.getCurrentWeather()


    suspend fun insertCurrentWeather(currentWeatherModel: CurrentWeatherModelToday) {
        currentWeatherDAO.insert(currentWeatherModel)
    }


    fun deleteCurrentWeather() {
        currentWeatherDAO.deleteAll()

    }
}