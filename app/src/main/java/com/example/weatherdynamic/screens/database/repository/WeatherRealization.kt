package com.example.project7.room.db.repository

import androidx.lifecycle.LiveData
import com.example.weatherdynamic.screens.database.dao.WeatherDao
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel

class WeatherRealization(private val weatherDao: WeatherDao)  {
    val allWeather: LiveData<List<WeatherModel>>
        get() = weatherDao.getAllWeather()


    suspend fun insertWeather(weatherModel: WeatherModel) {
        weatherDao.insert(weatherModel)
    }


    fun deleteAllWeather() {
        weatherDao.deleteAll()

    }
}