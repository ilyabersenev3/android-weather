package com.example.weatherdynamic.screens.database.dao


import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModelToday

@Dao
interface CurrentWeatherDaoToday {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun  insert(currentWeatherModelToday: CurrentWeatherModelToday)
    @Delete
    suspend fun delete(currentWeatherModelToday: CurrentWeatherModelToday)
    @Query("SELECT * from current_table ")
    fun getCurrentWeather(): LiveData<CurrentWeatherModelToday>

    @Query("DELETE FROM current_table")
    fun deleteAll()
}