package com.example.weatherdynamic.screens.database.dao


import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModel

@Dao
interface CurrentWeatherDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun  insert(currentWeatherModel: CurrentWeatherModel)
    @Delete
    suspend fun delete(currentWeatherModel: CurrentWeatherModel)
    @Query("SELECT * from current_table ")
    fun getCurrentWeather(): LiveData<CurrentWeatherModel>

    @Query("DELETE FROM current_table")
    fun deleteAll()
}