package com.example.weatherdynamic.screens.first

import com.example.weatherdynamic.data.repository.Repository
//import com.example.weatherdynamic.model.today.Today
import android.app.Application
import androidx.lifecycle.*
import android.widget.*
import com.example.project7.room.db.repository.WeatherRealization
import com.example.weatherdynamic.screens.WeatherDatabase
import com.example.weatherdynamic.screens.database.repository.CurrentRealization
import com.example.weatherdynamic.screens.database.repository.CurrentTodayRealisation
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModel
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModelToday
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.Dispatchers
//import retrofit2.Response

class FirstViewModel(application: Application) : AndroidViewModel(application) {

    /*var repo = Repository()
    val myToday: MutableLiveData<Response<Today>> = MutableLiveData()

    fun getTodayTemperature() {
        viewModelScope.launch {
            myToday.value = repo.getTodayCity("1","1")
        }
    }
     */

    private lateinit var lat: String
    private lateinit var lon: String
    private val context = application
    private var repository = Repository()

    private lateinit var repositoryDb: WeatherRealization
    private lateinit var repositoryCurrentDb: CurrentTodayRealisation



    fun initDatabase() {
        val daoWeather = WeatherDatabase.getInstance(context).getWeatherDao()
        repositoryDb = WeatherRealization(daoWeather)
        repositoryCurrentDb =
            CurrentTodayRealisation(WeatherDatabase.getInstance(context).getCurrentWeatherDaoTo())
    }

    fun getAllWeather(): LiveData<List<WeatherModel>> {
        return repositoryDb.allWeather
    }

    fun insert(weatherModel: WeatherModel) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryDb.insertWeather(weatherModel)
        }
    }

    fun insertCurrent(currentWeatherModelToday: CurrentWeatherModelToday) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCurrentDb.insertCurrentWeather(currentWeatherModelToday)
        }
    }

    fun getCurrent(): LiveData<CurrentWeatherModelToday> {
        return repositoryCurrentDb.currentWeather
    }

    fun delete() {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryDb.deleteAllWeather()
            repositoryCurrentDb.deleteCurrentWeather()
        }
    }
}