package com.example.weatherdynamic.screens.first

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherdynamic.databinding.FragmentFirstBinding
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.android.synthetic.main.fragment_first.view.*
import kotlinx.android.synthetic.main.item_temperature_lauout.view.*
import kotlinx.android.synthetic.main.item_today_layout.view.*

class FirstFragment : Fragment() {
/*    //lateinit var m_today: Today
    //lateinit var cardView: CardView
    lateinit var m_recyclerView: RecyclerView
    lateinit var m_adapter: FirstAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val viewModel = ViewModelProvider(this)[FirstViewModel::class.java]

        val v = inflater.inflate(R.layout.fragment_first, container, false)
        m_recyclerView = v.rv_first
        m_adapter = FirstAdapter()
        m_recyclerView.adapter = m_adapter

        viewModel.getTodayTemperature()

        viewModel.myToday.observe(viewLifecycleOwner) {
            list ->list.body()?.let { m_adapter.setToday(it) }
        }
        return v
    }

 */

    //private lateinit var bindingItem: ItemTodayLayoutBinding
    private lateinit var binding: FragmentFirstBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: FirstAdapter

    companion object {
        fun newInstance() = FirstFragment()
    }

    private lateinit var viewModel: FirstViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d("FirstFragment", "onCreateView(..")

        binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        viewModel = ViewModelProvider(this)[FirstViewModel::class.java]
        viewModel.initDatabase()

        adapter = FirstAdapter(object : DiffUtil.ItemCallback<WeatherModel>() {
            override fun areItemsTheSame(oldItem: WeatherModel, newItem: WeatherModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: WeatherModel, newItem: WeatherModel): Boolean {
                return oldItem.equals(newItem)
            }
        })

        recyclerView = binding.rvFirst
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        viewModel.getAllWeather().observe(viewLifecycleOwner) { listWeather ->
            adapter. submitList(listWeather)
        }

        viewModel.getCurrent().observe(viewLifecycleOwner) { item ->
            if (item != null) {
                binding.rvFirst.item_first_pressure.text = item.pressure.toString()
                binding.rvFirst.item_first_humidity.text = item.humidity.toString()+"%"
                binding.rvFirst.item_feels_like.text = item.feels_like.toString()
                binding.rvFirst.item_first_temperature.text = item.temperature.toString()+"°"
            }
        }

    }
}