package com.example.weatherdynamic.screens.database.repository

import androidx.lifecycle.LiveData
import com.example.weatherdynamic.screens.database.dao.CurrentWeatherDao
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModel


class CurrentRealization(private val currentWeatherDAO: CurrentWeatherDao) {
    val currentWeather: LiveData<CurrentWeatherModel>
        get() = currentWeatherDAO.getCurrentWeather()


    suspend fun insertCurrentWeather(currentWeatherModel: CurrentWeatherModel) {
        currentWeatherDAO.insert(currentWeatherModel)
    }


    fun deleteCurrentWeather() {
        currentWeatherDAO.deleteAll()

    }
}