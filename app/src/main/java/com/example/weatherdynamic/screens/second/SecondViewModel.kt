package com.example.weatherdynamic.screens.second

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.project7.room.db.repository.WeatherRealization
import com.example.weatherdynamic.data.repository.Repository
import com.example.weatherdynamic.screens.WeatherDatabase
import com.example.weatherdynamic.screens.database.repository.CurrentRealization
import com.example.weatherdynamic.screens.database.repository.CurrentTodayRealisation
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModel
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModelToday
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SecondViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var lat: String
    private lateinit var lon: String
    private val context = application
    private var repository = Repository()

    private lateinit var repositoryDb: WeatherRealization
    private lateinit var repositoryCurrentDb: CurrentRealization
    private lateinit var repositoryCurrentDbTo: CurrentTodayRealisation


    fun getTemperature(city: String) {
        viewModelScope.launch {
            val response = repository.getLocationCity(city)
            lat = response[0].lat.toString()
            lon = response[0].lon.toString()

            delete()
            val info1 = repository.getWeekCity(lat, lon)

            val date1 = info1.current.dt
            val humidity1 = info1.current.humidity
            val pressure1 = info1.current.pressure
            val temp1 = info1.current.temp
            val feels_like1 = info1.current.feels_like

            val info2 = repository.getTodayCity(lat, lon)

            val humidity2 = info2.main.humidity
            val pressure2 = info2.main.pressure
            val temp2 = info2.main.temp
            val feels_like2 = info2.main.feels_like

            insertCurrent(
                CurrentWeatherModel(
                    temperature = temp1,
                    date = date1,
                    humidity = humidity1,
                    pressure = pressure1,
                    feels_like = feels_like1
                )
            )
            insertCurrentToday(
                CurrentWeatherModelToday(
                    temperature = temp2,
                    humidity = humidity2,
                    pressure = pressure2,
                    feels_like = feels_like2
                )
            )
            for (i in 1..5) {
                val humidityDaily1 = info1.daily[i].humidity
                val dataDaily1 = info1.daily[i].dt
                val pressureDaily1 = info1.daily[i].pressure
                val tempDaily1 = info1.daily[i].temp
                insert(
                    WeatherModel(
                        temperature = tempDaily1.day,
                        date = dataDaily1,
                        humidity = humidityDaily1,
                        pressure = pressureDaily1
                    )
                )
            }
        }
    }


    fun initDatabase() {
        val daoWeather = WeatherDatabase.getInstance(context).getWeatherDao()
        repositoryDb = WeatherRealization(daoWeather)
        repositoryCurrentDb =
            CurrentRealization(WeatherDatabase.getInstance(context).getCurrentWeatherDao())
    }

    fun getAllWeather(): LiveData<List<WeatherModel>> {
        return repositoryDb.allWeather
    }

    fun insert(weatherModel: WeatherModel) =
        viewModelScope.launch(Dispatchers.IO) {
            repositoryDb.insertWeather(weatherModel)
        }

    fun insertCurrent(currentWeatherModel: CurrentWeatherModel) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCurrentDb.insertCurrentWeather(currentWeatherModel)
        }
    }

    fun insertCurrentToday(currentWeatherModelToday: CurrentWeatherModelToday) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryCurrentDbTo.insertCurrentWeather(currentWeatherModelToday)
        }
    }

    fun getCurrent(): LiveData<CurrentWeatherModel> {
        return repositoryCurrentDb.currentWeather
    }

    fun delete() =
        viewModelScope.launch(Dispatchers.IO) {
            repositoryDb.deleteAllWeather()
            repositoryCurrentDb.deleteCurrentWeather()
        }
}