package com.example.weatherdynamic.screens.first

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherdynamic.R
import com.example.weatherdynamic.model.today.Today
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.android.synthetic.main.item_today_layout.view.*

class FirstAdapter(diffCallback: DiffUtil.ItemCallback<WeatherModel>)
    :RecyclerView.Adapter<FirstAdapter.FirstViewHolder>() {

    private lateinit var m_today: Today

    class FirstViewHolder(view: View): RecyclerView.ViewHolder(view){
        val temperature=view.findViewById<TextView>(R.id.item_first_temperature)
        val humidity=view.findViewById<TextView>(R.id.item_first_humidity)
        val pressure=view.findViewById<TextView>(R.id.item_first_pressure)
        val feelsLike=view.findViewById<TextView>(R.id.item_feels_like)
        val wind=view.findViewById<TextView>(R.id.item_first_wind)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_today_layout, parent, false)
        return FirstViewHolder(view)
    }

    override fun onBindViewHolder(holder: FirstViewHolder, position: Int) {

        holder.itemView.item_first_temperature.text = m_today.main.temp.toString()
        holder.itemView.item_first_humidity.text = m_today.main.humidity.toString()
        holder.itemView.item_first_pressure.text = m_today.main.pressure.toString()
        holder.itemView.item_feels_like.text = m_today.main.pressure.toString()
    }

    override fun getItemCount(): Int {
        return 1
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setToday(v: Today){
        m_today = v
        notifyDataSetChanged()
    }
}