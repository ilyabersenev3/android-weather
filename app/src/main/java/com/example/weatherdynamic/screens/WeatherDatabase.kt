package com.example.weatherdynamic.screens

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.weatherdynamic.screens.database.dao.CurrentWeatherDao
import com.example.weatherdynamic.screens.database.dao.CurrentWeatherDaoToday
import com.example.weatherdynamic.screens.database.dao.WeatherDao
import com.example.weatherdynamic.screens.modelDataBase.CurrentWeatherModel
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel

@Database(
    entities = [
        WeatherModel::class,
        CurrentWeatherModel::class
    ], version = 1
)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun getWeatherDao(): WeatherDao

    companion object {
        private var database: WeatherDatabase? = null

        @Synchronized
        fun getInstance(context: Context): WeatherDatabase {
            return if (database == null) {
                database = Room.databaseBuilder(context, WeatherDatabase::class.java, "db").build()
                database as WeatherDatabase
            } else {
                database as WeatherDatabase
            }
        }

    }

    abstract fun getCurrentWeatherDao(): CurrentWeatherDao
    abstract fun getCurrentWeatherDaoTo(): CurrentWeatherDaoToday


}