package com.example.weatherdynamic.screens.second

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherdynamic.databinding.FragmentSecondBinding
import com.example.weatherdynamic.screens.modelDataBase.WeatherModel
import kotlinx.android.synthetic.main.item_temperature_lauout.view.*

class SecondFragment : Fragment() {
    private lateinit var binding: FragmentSecondBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: SecondAdapter

    companion object {
        fun newInstance() = SecondFragment()
    }

    private lateinit var viewModel: SecondViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    private fun init() {
        viewModel = ViewModelProvider(this)[SecondViewModel::class.java]
        viewModel.initDatabase()
        recyclerView = binding.rvSecond
        adapter = SecondAdapter(object : DiffUtil.ItemCallback<WeatherModel>() {
            override fun areItemsTheSame(oldItem: WeatherModel, newItem: WeatherModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: WeatherModel, newItem: WeatherModel): Boolean {
                return oldItem.equals(newItem)
            }

        })

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        viewModel.getAllWeather().observe(viewLifecycleOwner) { listWeather ->
            adapter.submitList(listWeather)
        }

        viewModel.getCurrent().observe(viewLifecycleOwner) { item ->
            if (item != null) {
                binding.rvSecond.item_temperature.text = item.temperature.toString()+"°"
                binding.rvSecond.item_humidity.text = item.humidity.toString()+"%"
                binding.rvSecond.item_pressure.text = item.pressure.toString()
                binding.rvSecond.item_date.text = item.date.toString()
            }
        }

        binding.button.setOnClickListener {
            if (binding.editTextTextPersonName.text.toString().trim() == "") {
                Toast.makeText(activity, "Введите город", Toast.LENGTH_LONG).show()
            } else {
                viewModel.getTemperature(binding.editTextTextPersonName.text.toString())
            }
        }
    }
}