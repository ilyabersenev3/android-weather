package com.example.weatherdynamic.data.repository

import com.example.weatherdynamic.data.api.RetrofitInstance
import com.example.weatherdynamic.model.Geoposition.GeoposItem
import com.example.weatherdynamic.model.today.Today
import com.example.weatherdynamic.model.week.week
import retrofit2.Response

class Repository {
    suspend fun getTodayCity(lat:String,lon:String): Today {
        return  RetrofitInstance.api.getWeatherToday(lat,lon)
    }

    suspend fun getWeekCity(lat:String,lon:String): week {
        return  RetrofitInstance.api.getWeatherWeek(lat,lon)
    }

    suspend fun getLocationCity(city:String): ArrayList<GeoposItem> {
        return RetrofitInstance.api.getLocation(city)
    }
}