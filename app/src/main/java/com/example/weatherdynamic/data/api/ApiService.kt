package com.example.weatherdynamic.data.api

import com.example.weatherdynamic.model.Geoposition.GeoposItem
import com.example.weatherdynamic.model.today.Today
import com.example.weatherdynamic.model.week.week
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    /*var KEY: String
        get() = "b229a76acbfd78dc5bba169d142c1b75"
        set(value) = TODO() */

    @GET("geo/1.0/direct?limit=5&appid=b229a76acbfd78dc5bba169d142c1b75")
    suspend fun getLocation(@Query("q")  city:String ):ArrayList<GeoposItem>

    @GET("data/2.5/weather?&lang=ru&units=metric&appid=b229a76acbfd78dc5bba169d142c1b75")
    suspend fun getWeatherToday(@Query("lat")  lat:String ,
                           @Query("lon") lon:String
    ):Today

    @GET("data/2.5/onecall?&units=metric&lang=ru&exclude=minutely,hourly,alerts&appid=b229a76acbfd78dc5bba169d142c1b75")
    suspend fun getWeatherWeek(@Query("lat")  lat:String ,
                           @Query("lon") lon:String
    ):week

    /*
    @GET("data/2.5/weather?q=Omsk&lang=ru&units=metric&appid=b229a76acbfd78dc5bba169d142c1b75")
    suspend fun getToday(): Response<Today>

     */

}