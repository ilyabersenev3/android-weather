package com.example.weatherdynamic.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    val BASE_URL = "https://api.openweathermap.org/"

/*
    fun create() : ApiService {
        val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        return retrofit.create(ApiService::class.java)
    }
*/

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        }

    val api: ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }

}