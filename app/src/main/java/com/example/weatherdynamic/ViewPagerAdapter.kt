package com.example.weatherdynamic

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.weatherdynamic.screens.first.FirstFragment
import com.example.weatherdynamic.screens.second.SecondFragment

class ViewPagerAdapter(framentActivity: FragmentActivity):FragmentStateAdapter(framentActivity) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> FirstFragment()
            else -> SecondFragment()
        }
    }
}